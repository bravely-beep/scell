
/*

Example program utilizing type synonyms in a module
to hide type constructors

 */
// No lifetimes!
pub struct ComplexStruct(
    resource::Locked<Vec<u32>>,
    resource::Locked<String>,
);

pub struct Env<'a> {
    // Zero-sized proof of access
    pub w: resource::WriteAccess<'a>,
}

pub fn main() {
    // This could also be done as e.g. a lazy static
    // (see locked.rs for an example)
    // but we'll just create the lock in main
    let lock = std::sync::Mutex::new(
        singleton_trait::Erased::new(
            resource::Lock::new().expect("In main")));

    let ds = ComplexStruct(
        resource::Locked::new(vec![0, 1]),
        resource::Locked::new(String::new()));
    {
        let mut locked = lock.try_lock().unwrap();

        inc_all(locked.borrow_mut(), &ds);
        let result = read_values(locked.borrow(), &ds);

        println!("Result was {}", result);

        let _ = Env {w: locked.borrow_mut() };
    }
}

fn read_values(r: resource::ReadAccess, c: &ComplexStruct) -> u32 {
    let vec = c.0.borrow(r);
    vec[0] + vec[1]
}

fn inc_all(mut w: resource::WriteAccess, c: &ComplexStruct) {
    for x in c.0.borrow_mut(&mut w) {
        *x += 1
    }
    c.1.borrow_mut(w).push_str("Hello!");
}

/**
 * Control access to some shared memory resource
 */
pub mod resource {
    use singleton_cell::{new_singleton, SCell};
    use singleton_trait::{Singleton, Erased};

    new_singleton!(pub Lock);

    pub type Locked<T> = SCell<Lock, T>;
    // zero-sized acess tokens
    pub type ReadAccess<'a> = Erased<&'a Lock>;
    pub type WriteAccess<'a> = Erased<&'a mut Lock>;
}
