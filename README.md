
[![Version 0.3.1][version-badge]][changelog] [![MIT License][license-badge]][license]

[changelog]: ./CHANGELOG.md
[license]: ./LICENSE
[version-badge]: https://img.shields.io/badge/version-0.3.1-blue.svg
[license-badge]: https://shields.io/badge/license-MIT-green.svg


This library provides a safe, zero-overhead interface for guarding access
to shared data via access to another singleton token. It is an extension of GhostCell to allow more general singletons besides branded tokens,
enabling data to be `'static`

This library also provides two singleton implementations by itself:

* A scoped, branded token as GhostCell via `with_token`
* Simple create-once singleton structs via `new_singleton`

# Why SCell over:
## GhostCell (and LCell from the qcell crate)

* With a concrete singleton type, SCell's can be `'static` and thus stored in `'static` data-structures.
* SCell usages do not have to be strictly scoped.
* SCell is strictly more flexible and provides a `with_token` method to emulate GhostCell.
* Erased zero-size references can be used with SCell, allowing truly zero-cost abstraction.

## Other Cells from the qcell crate

* Creating a SCell does not require any reference to the owning type.
* SCells provide `from_mut` and similar interfaces: they are structural like Cell and GhostCell.
* SCells have no runtime cost.

# Why not?

* Unlike GhostCell, more general usages for SCell have not been formally proven correct.
* Type signatures involving SCell can be slightly more verbose than other options.

# Examples

See the examples directory for some code samples
```
new_singleton(pub Lock);
pub fn main() {
    let mut lock1 = Lock::new().expect("Called main twice");
    let cell1 = SCell::new(0);
    let cell2 = &cell1;

    cell1.borrow_mut(&mut lock1) += 1;

    assert_eq!(1, *cell2.borrow(&lock1));
}
```
